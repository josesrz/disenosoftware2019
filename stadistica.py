from math import *

def mean(list):
    sum = 0
    for i in list:
        sum += i
    mean = sum / len(list)

    return mean

def standardDeviation(list):
    sum = 0

    for i in list:
        sum += i
    mean = sum / len(list)

    sum_sd = 0
    for i in list:
        sum_sd += (i - mean)**2

    sum_sd = sum_sd / len(list)
    sd = sqrt(sum_sd)

    return sd

def median(list):
    list.sort()
    i_median = len(list)
    print(list)
    if i_median % 2 == 0:
        print(i_median)
        i_median = int(i_median/2)
        value = (list[i_median] + list[i_median + 1]) / 2
    else:
        value = list[i_median//2]

    return value


def quartile(list):
    list.sort()
    quartiles = []
    median_list = median(list)
    index = list.index(median_list)
    quartiles.append(median(list))

    first_quartile = median(list[:index])
    quartiles.append(first_quartile)

    third_quartile = median(list[index:])
    quartiles.append(third_quartile)

    counter = 1
    for i in quartiles:
        print("{} quartile: {}".format(counter, i))
        counter += 1

    return quartiles

def percentile(p,list):

    return (p / 100)*(len(list)+1)
