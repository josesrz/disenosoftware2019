"""
LIBRARIES
"""

from math import *
from random import *
import stadistica


""" Ask the user for a number. Depending on whether the number is even or
odd, print out an appropriate message to the user. Hint: how does an even /
odd number react differently when divided by 2?
If the number is a multiple of 4, print out a different message.
Ask the user for two numbers: one number to check (call it num) and one
number to divide by (check). If check divides evenly into num, tell that to the
user. If not, print a different appropriate message.
"""

user_number = int(input("Write a number\n"))

if(user_number % 4 == 0):
    print("The number is even and also divisible by four!\n")

elif(user_number % 2 == 0):
    print("The number is even \n")
else:
    print("The number you typed is odd :( \n")

user_number_two = int(input("Type another number\n"))
user_number_three = int(input("Type a number you want to divide by\n"))

if(user_number_two % user_number_three == 0):
    print("The number {} is divisible by {}".format(user_number_two,user_number_three))
else:
    print("The number {} is NOT divisible by {} :( ".format(user_number_two, user_number_two))


"""
Generate a random number between 1 and 9 (including 1 and 9). Ask the
user to guess the number, then tell them whether they guessed too low, too
high, or exactly right. (Hint: remember to use the user input lessons from the
very first exercise)
Keep the game going until the user types “exit”
Keep track of how many guesses the user has taken, and when the game
ends, print this out.
"""

print("Try to guess the number between 0 and 9")

random_number = randint(0,9)
guess = False

while guess == False:
    user_guess = int(input("Type a number between 0 and 9\n"))
    if(user_guess == random_number):
        print("You guessed the number! Congrats!\n")
        break
    elif(user_guess < random_number ):
        print("To low! Try again")
        pass
    else:
        print("To high! Try again\n")
        pass

"""
Write a program (function!) that takes a list and returns a new list that
contains all the elements of the first list minus all the duplicates.
Write two different functions to do this - one using a loop and constructing a
list, and another using sets
"""

def contruct_list():
    characters = input("Enter a letter or number seperated by a space\n")
    characters_list = characters.split(" ")
    return characters_list

def check_doubles(list1, list2):
    new_list = list1 + list2

    return list(set(new_list))



list_one = contruct_list()
list_two = contruct_list()

new_list = check_doubles(list_one, list_two)

print(new_list)

"""
Write a function that computes the standard deviation for a set of numbers
coming from a list. Do not use any math module, compute the algorithm
"""

example_list = [1, 2, 3, 4, 5, 5, 5, 6, 7, 8, 8, 8, 9, 0, 1, 2, 3]

def standardDeviation(list):
    sum = 0

    for i in list:
        sum += i
    mean = sum / len(list)

    sum_sd = 0
    for i in list:
        sum_sd += (i - mean)**2

    sum_sd = sum_sd / len(list)
    sd = sqrt(sum_sd)

    return sd

print(standardDeviation(example_list))


"""
Write a function that receives as parameters how many Fibonnaci numbers
to generate and then generates them. Take this opportunity to think about
0,0.5,1,3,8,2000,450000,-1,p,[]
how you can use functions. Make sure to ask the user to enter the number
of numbers in the sequence to generate.(Hint: The Fibonnaci sequence is a
sequence of numbers where the next number in the sequence is the sum of
the previous two numbers in the sequence. The sequence looks like this: 1,
1, 2, 3, 5, 8, 13, …)
# """

def fibonnaci(n):
    if n <= 1 :
        return n
    else:
        return fibonnaci(n-1) + fibonnaci(n-2)

numbers = 5
fibonacci_list = []

for i in range(numbers):
    print(fibonnaci(i))

print(fibonacci_list)
print(fibonnaci(4))


"""
Write a function that evaluates if a given list satisfy Fibonacci sequence
returning true or false if the list satisfy the criteria
"""

fibonacci_list_corecct = [0,1,1,2,3] #when number is 4

fibonacci_list_incorrect = [0, 1, 2, 4]

def check_fibonacci(list1):
    n = len(list1)
    fibonacci_list = []
    for i in range(n):
        fibonacci_list.append(fibonnaci(i))

    return fibonacci_list == list1

print(check_fibonacci(fibonacci_list_incorrect)) #Prints false
print(check_fibonacci(fibonacci_list_corecct)) #Prints true


"""
Write a password generator function in Python. Be creative with how you
generate passwords - strong passwords have a mix of lowercase letters,
uppercase letters, numbers, and symbols. The passwords should be random,
generating a new password every time the user asks for a new password. 
"""

def passwordGeneration(length):
    caps_list = ['A','B', 'C', 'D', 'F', 'G', 'H', 'I', 'J', 'K', 'M', 'N', 'L', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
                 'W', 'X', 'Y', 'Z']
    shuffle(caps_list)

    letters = ['a','b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'l', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                 'w', 'x', 'y', 'z']
    shuffle(letters)

    characters = ['~', '!', '#', '%', '^', '&', '*', '(', ')', '+', '.', '/', '?', '=']
    shuffle(characters)

    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    shuffle(numbers)

    random_list = [characters, letters, numbers, caps_list]

    password = []

    counter = 0

    while counter <= length:
        counter += 1
        random_i = randint(0,3)
        password.append(str(random_list[random_i].pop(0)))

    return ''.join(password)

print(passwordGeneration(20))
print(passwordGeneration(10))
print(passwordGeneration(5))


"""
Write a module containing different function that computes the
1. Sample mean
2. Sample standard deviation
3. Median
4. A function that returns the n-quartil
5. A function that returns the n-percentil
"""

example_list = [1, 2, 3, 4, 5, 5, 5, 6, 7, 8, 8, 8, 9, 0, 1, 2, 3]

print("Mediana: {}".format(stadistica.median(example_list)))
print("Media: {}".format(stadistica.mean(example_list)))
print("Quartiles: {}".format(stadistica.quartile(example_list)))
print("Standar Deviation: {}".format((stadistica.standardDeviation(example_list))))
print("Precentil 30 :{}".format(stadistica.percentile(30,example_list)))


"""
Write a function that converts a decimal number into a Roman format
"""

numeral_map = zip(
    (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
    ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
)

def int_to_roman(i):
    result = []
    for integer, numeral in numeral_map:
        count = int(i / integer)
        result.append(numeral * count)
        i -= integer * count
    return ''.join(result)

print(int_to_roman(967))